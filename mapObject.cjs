let testObject = require('./testObject.cjs');

function mapObject (testObject, callBack){

    let testMap ={};
    for(let key in testObject)
    {
        let value= testObject[key];
        let updatedValue = callBack(value);
        
        testMap[key]= updatedValue;
        
    }
    return testMap;
}

module.exports = mapObject;