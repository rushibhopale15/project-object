
function pairs(testObject) {
    let list = [];
    if (typeof testObject === 'object') {
        for (let key in testObject) {
            list.push([key, testObject[key]]);
        }
    }
    return list;
}
module.exports = pairs;