let testObject = require('./testObject.cjs');


function invert(testObject)
{
    let copyObject={};
    if(typeof testObject === 'object')
    {
        for(let key in testObject)
        {
            copyObject[testObject[key]] = key;
        }
    }
    return copyObject;
}

module.exports= invert;