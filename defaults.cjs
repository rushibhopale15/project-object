let testObject = require('./testObject.cjs');

function defaults(newObject, defaultProps)
{
    for(let key in defaultProps)
    {
        if(newObject[key] === undefined ){
            newObject[key] = defaultProps[key];
        }
    }
    return newObject;
}

module.exports = defaults;